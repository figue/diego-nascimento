/*
 * Copyright (C) 2018 Diego Figueredo do Nascimento.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.dfn.desafioandroid.features.base

import android.content.Context
import android.support.v4.app.Fragment

/**
 * Base Fragment.
 *
 * This abstract class has a base implementation to Fragments, all present should extend from this
 *
 * @param V the type of a interface that represent View in MVP partner.
 * @param P the type of a interface that represent Presenter in MVP partner.
 * @property mPresenter instance of Presenter attach on activity
 * @property mView instance of View attach on activity
 * automatically.
 */
abstract class BaseFragment<P : BasePresenter<V>, V : BaseView> : Fragment(), BaseView {
    lateinit var mPresenter: P
    lateinit var mView: V

    override fun onAttach(context: Context) {
        inject()
        super.onAttach(context)
    }

    override fun onDestroy() {
        super.onDestroy()
        detachPresenter()
    }

    private fun inject() {
        onCreateView()
        setView(getFragmentView())
        attachPresenter()
    }

    private fun setView(view: V) {
        this.mView = view
    }

    abstract fun getFragmentView(): V

    abstract fun onCreateView()

    private fun attachPresenter() {
        mPresenter.attachView(mView)
    }

    private fun detachPresenter() {
        mPresenter.detachView()
    }

    override fun onShowProgress(show: Boolean) {
    }

    override fun onShowError() {
    }

    override fun onShowMessage(msg: String) {
    }
}