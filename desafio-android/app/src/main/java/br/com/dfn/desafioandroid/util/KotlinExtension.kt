/*
 * Copyright (C) 2018 Diego Figueredo do Nascimento.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.dfn.desafioandroid.util

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import com.squareup.picasso.Picasso


fun ImageView.loadImage(url: String) {
    Picasso.get().load(url).into(this)
}

fun RecyclerView.onScrollToEnd(linearLayoutManager: LinearLayoutManager, adapter: RecyclerView.Adapter<*>,
                               onScrollNearEnd: (Unit) -> Unit) =
        addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                //if scrolled down
                if (dy > 0) {
                    val lastVisibleItemPosition = linearLayoutManager.findLastVisibleItemPosition()

                    if (lastVisibleItemPosition == adapter.itemCount - 1) {
                        onScrollNearEnd(Unit)
                    }
                }
            }
        })