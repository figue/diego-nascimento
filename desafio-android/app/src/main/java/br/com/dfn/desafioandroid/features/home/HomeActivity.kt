/*
 * Copyright (C) 2018 Diego Figueredo do Nascimento.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.com.dfn.desafioandroid.features.home

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import br.com.dfn.desafioandroid.R
import br.com.dfn.desafioandroid.data.source.local.model.RepositoryDb
import br.com.dfn.desafioandroid.di.Injection
import br.com.dfn.desafioandroid.features.base.BaseActivity
import br.com.dfn.desafioandroid.features.base.BaseViewModel
import br.com.dfn.desafioandroid.features.home.adapters.RepositoryRecycleAdapter
import br.com.dfn.desafioandroid.features.pullrequests.PullRequestActivity
import br.com.dfn.desafioandroid.util.onScrollToEnd
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.component_loading.*

class HomeActivity : BaseActivity<HomePresenter, HomeContract.HomeView>(), HomeContract.HomeView,
        RepositoryRecycleAdapter.OnRecycleViewListener {

    private var isLoading = false
    private lateinit var mAdapter: RepositoryRecycleAdapter

    override fun onRequestLayout(): Int {
        return R.layout.activity_home
    }

    override fun onCreate() {
        val mViewModel = ViewModelProviders.of(this).get(BaseViewModel::class.java)

        if (mViewModel.mPresenter == null) {
            mPresenter = HomePresenter(Injection.providesContext(), Injection.providesAndroidSchedulers())
            mPresenter.attachLifecycle(lifecycle)
            mViewModel.mPresenter = mPresenter

        } else {
            mPresenter = mViewModel.mPresenter as HomePresenter
        }

        buildToolbar()
    }

    private fun buildToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setIcon(R.drawable.ic_menu)
    }

    override fun onResume() {
        super.onResume()
        mPresenter.doRequestRepository(false, isConnected())
    }

    override fun getView(): HomeContract.HomeView {
        return this
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        lastFirstVisiblePosition = (recyclerView.layoutManager as? LinearLayoutManager)?.findFirstCompletelyVisibleItemPosition() ?: 0
        outState?.putInt(lastPosition, lastFirstVisiblePosition)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        lastFirstVisiblePosition = savedInstanceState?.getInt(lastPosition, 0) ?: 0
    }

    override fun onClick(rep: RepositoryDb) {
        Intent(applicationContext, PullRequestActivity::class.java).apply {
            putExtra(PullRequestActivity.loginParam, rep.login)
            putExtra(PullRequestActivity.repParam, rep.name)
            startActivity(this)
        }
    }

    override fun onShowRepositories(repositories: List<RepositoryDb>) {
        onShowProgress(false)
        recyclerView.setHasFixedSize(false)

        val layoutManager = LinearLayoutManager(applicationContext)
        recyclerView.layoutManager = layoutManager
        mAdapter = RepositoryRecycleAdapter(repositories, this)
        recyclerView.adapter = mAdapter

        (recyclerView.layoutManager as LinearLayoutManager).scrollToPosition(lastFirstVisiblePosition)
        recyclerView.onScrollToEnd(recyclerView.layoutManager as LinearLayoutManager,
                recyclerView.adapter, { onScrollToEnd() })

    }

    private fun onScrollToEnd() {
        if (!isLoading && isConnected()) {
            lastFirstVisiblePosition =
                    (recyclerView.layoutManager as? LinearLayoutManager)?.findFirstCompletelyVisibleItemPosition() ?: 0
            mPresenter.doRequestRepository(true)
        }
    }

    override fun onShowProgress(show: Boolean) {
        isLoading = show
        val visibility = if (show) View.VISIBLE else View.GONE
        loading.visibility = visibility
    }

    override fun onShowError() {
    }

    override fun onShowMessage(msg: String) {
    }

}
