/*
 * Copyright (C) 2018 Diego Figueredo do Nascimento.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.dfn.desafioandroid.data.source.remote

import br.com.dfn.desafioandroid.data.model.GitHubRepositoryResult
import br.com.dfn.desafioandroid.data.model.PullRequest
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface GitHubApi {
    @GET("search/repositories?q=language:Java&sort=stars")
    fun getRepositories(@Query("page") page: Int): Observable<GitHubRepositoryResult>

    @GET("repos/{login}/{rep}/pulls")
    fun getPullRequest(@Path("login") login: String, @Path("rep") rep: String): Observable<List<PullRequest>>
}