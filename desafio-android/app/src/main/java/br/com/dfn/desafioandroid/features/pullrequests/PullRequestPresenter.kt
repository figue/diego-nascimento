/*
 * Copyright (C) 2018 Diego Figueredo do Nascimento.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.dfn.desafioandroid.features.pullrequests

import android.util.Log
import br.com.dfn.desafioandroid.data.model.PullRequest
import br.com.dfn.desafioandroid.data.source.remote.GitHubClient
import br.com.dfn.desafioandroid.features.base.BasePresenter
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers


class PullRequestPresenter() : BasePresenter<PullRequestContract.PullRequestView>() {

    private var mList: MutableList<PullRequest> = ArrayList()
    private var mScheduler: Scheduler? = null
    private val stateOpen = "open"

    constructor(scheduler: Scheduler) : this() {
        mScheduler = scheduler
    }

    fun doGetPullRequest(login: String, repository: String) {

        var view = getView()
        if (mList.isNotEmpty()) {
            view?.onShowPullRequest(mList)
            return
        }

        view?.onShowProgress(true)

        var gitHubApi = GitHubClient
        var disposable = gitHubApi.getApi().getPullRequest(login, repository)
                .subscribeOn(Schedulers.io())
                .observeOn(mScheduler)
                .subscribe(
                        { result ->
                            result.forEach {
                                mList.add(it)
                            }
                            calculatePullRequests(mList)
                            view?.onShowPullRequest(mList)
                        },
                        { t: Throwable -> handleError(t) },
                        { handleComplete() })

        mCompositeDisposable.add(disposable)
    }

    fun calculatePullRequests(list: MutableList<PullRequest>) {
        var opens = 0
        var closes = 0

        list.forEach { rep -> if (rep.state == stateOpen) opens++ else closes++ }

        getView()?.onShowPullRequestStates(opens, closes)
    }


    private fun handleComplete() {
        Log.d("handleComplete", "handleComplete")
    }

    private fun handleError(t: Throwable) {
        Log.d("handleError", "Throwable: " + t.message)
        getView()?.onShowError()
    }
}