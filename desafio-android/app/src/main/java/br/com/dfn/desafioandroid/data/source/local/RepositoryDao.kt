/*
 * Copyright (C) 2018 Diego Figueredo do Nascimento.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.dfn.desafioandroid.data.source.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import br.com.dfn.desafioandroid.data.source.local.model.RepositoryDb
import io.reactivex.Single

@Dao
interface RepositoryDao {
    @Query("SELECT * from repositorydb")
    fun getAll(): Single<MutableList<RepositoryDb>>

    @Insert(onConflict = REPLACE)
    fun insert(repositoryDb: RepositoryDb)

    @Query("DELETE from repositorydb")
    fun deleteAll()
}