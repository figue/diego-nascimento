/*
 * Copyright (C) 2018 Diego Figueredo do Nascimento.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.dfn.desafioandroid.features.home

import android.content.Context
import android.util.Log
import br.com.dfn.desafioandroid.data.source.local.AppDatabase
import br.com.dfn.desafioandroid.data.source.local.model.RepositoryDb
import br.com.dfn.desafioandroid.data.source.remote.GitHubClient
import br.com.dfn.desafioandroid.features.base.BasePresenter
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers


class HomePresenter() : BasePresenter<HomeContract.HomeView>() {

    private var mContext: Context? = null
    private var mScheduler: Scheduler? = null
    private var mDb: AppDatabase? = null
    var mListRepositoryDb: MutableList<RepositoryDb> = ArrayList()
    private var mPage = 0
    private lateinit var mObservable: Observable<MutableList<RepositoryDb>>

    constructor(context: Context, scheduler: Scheduler) : this() {
        mContext = context
        mScheduler = scheduler
        mDb = AppDatabase.getInstance(context)
    }

    fun doRequestRepository(forceRequest: Boolean, fromWeb: Boolean = true) {

        var view = getView()
        if (mListRepositoryDb.isNotEmpty() && !forceRequest) {
            view?.onShowRepositories(mListRepositoryDb)
            return
        }
        view?.onShowProgress(true)

        mObservable = if (fromWeb) getGitHubObservable() else getGitHubDbObservable()

        var disposable = mObservable.observeOn(mScheduler)
                .subscribe(
                        { result -> view?.onShowRepositories(result) },
                        { t: Throwable -> this.handleError(t) },
                        { handleComplete() })

        mCompositeDisposable.add(disposable)
    }

    private fun getGitHubObservable(): Observable<MutableList<RepositoryDb>> {
        var gitHubApi = GitHubClient
        mPage++

        return gitHubApi.getApi().getRepositories(mPage)
                .subscribeOn(Schedulers.io())
                .flatMap { result ->
                    result.items.forEach { rep ->
                        val repDb = RepositoryDb.newInstance(rep)
                        mDb?.repositoryDao()?.insert(repDb)
                        mListRepositoryDb.add(repDb)
                    }
                    Observable.just(mListRepositoryDb)
                }
    }

    private fun getGitHubDbObservable(): Observable<MutableList<RepositoryDb>> {
        return mDb!!.repositoryDao().getAll().toObservable().subscribeOn(Schedulers.io())
    }

    private fun handleComplete() {
        Log.d("handleComplete", "handleComplete")
    }

    private fun handleError(t: Throwable) {
        Log.d("handleError", "Throwable: " + t.message)
    }
}