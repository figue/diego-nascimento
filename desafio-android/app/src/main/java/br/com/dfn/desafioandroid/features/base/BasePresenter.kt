/*
 * Copyright (C) 2018 Diego Figueredo do Nascimento.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.dfn.desafioandroid.features.base

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.util.Log
import io.reactivex.disposables.CompositeDisposable
import java.lang.ref.WeakReference

/**
 * Base Presenter.
 *
 * This class has a base implementation to Presenter, all present should extend from this
 *
 * @param V the type of a interface that represent View in MVP partner.
 * @property view the weakReference of view to be attach and detach to avoid memoryLeaks problems
 * @property viewAttachedAtLeastOnce a flag to control view state attach or not attach
 */
abstract class BasePresenter<V> : LifecycleObserver {
    private var view: WeakReference<V>? = null
    private var viewAttachedAtLeastOnce = false

    var mCompositeDisposable = CompositeDisposable()

    /**
     * Method to attach view at presenter
     *
     * */
    fun attachView(view: V) {
        this.view = WeakReference(view)
        this.viewAttachedAtLeastOnce = true
    }

    /**
     * Method to detach view at presenter
     *
     * */
    fun detachView() {
        mCompositeDisposable.clear()
        if (this.view != null) {
            this.view!!.clear()
            this.view = null
        }
    }

    /**
     * Method to registry observable lifecycle from architecture components
     *
     * */
    fun attachLifecycle(lifecycle: Lifecycle) {
        lifecycle.addObserver(this)
    }

    /**
     * Method to remove observable lifecycle from architecture components
     *
     * */
    fun detachLifecycle(lifecycle: Lifecycle) {
        lifecycle.removeObserver(this)
    }

    /**
     * Method that return a view attach on presenter
     *
     * */
    fun getView(): V? {
        if (!viewAttachedAtLeastOnce) {
            throw IllegalStateException("No view has ever been attached to this presenter!")
        }

        return view!!.get()
    }


    /**
     * Method that represent "onCreate" from lifecycle activity.
     * This method must be override by own presenter whenever necessary.
     *
     * */
    @OnLifecycleEvent(value = Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        Log.d("Presenter", "onCreate")
    }

    /**
     * Method that represent "OnResume" from lifecycle activity.
     * This method must be override by own presenter whenever necessary.
     *
     * */
    @OnLifecycleEvent(value = Lifecycle.Event.ON_RESUME)
    fun onResume() {
        Log.d("Presenter", "OnResume")
    }

    /**
     * Method that represent "onPause" from lifecycle activity.
     * This method must be override by own presenter whenever necessary.
     *
     * */
    @OnLifecycleEvent(value = Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        Log.d("Presenter", "onPause")
    }

    /**
     * Method that represent "onDestroy" from lifecycle activity.
     * This method must be override by own presenter whenever necessary.
     *
     * */
    @OnLifecycleEvent(value = Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        Log.d("Presenter", "onDestroy")
    }

}