/*
 * Copyright (C) 2018 Diego Figueredo do Nascimento.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.dfn.desafioandroid.data.source.local.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import br.com.dfn.desafioandroid.data.model.Repository

@Entity
class RepositoryDb(
        @PrimaryKey
        @ColumnInfo(name = "id")
        var id: Int,
        @ColumnInfo(name = "name")
        var name: String? = "",
        @ColumnInfo(name = "description")
        var description: String? = "",
        @ColumnInfo(name = "forksCount")
        var forksCount: Int = 0,
        @ColumnInfo(name = "starsCount")
        var starsCount: Int = 0,
        @ColumnInfo(name = "fullName")
        var fullName: String = "",
        @ColumnInfo(name = "login")
        var login: String? = "",
        @ColumnInfo(name = "avatar")
        var avatar: String = "") {

    companion object {
        fun newInstance(repository: Repository): RepositoryDb {
            return RepositoryDb(repository.id,
                    repository.name,
                    repository.description,
                    repository.forksCount,
                    repository.starsCount,
                    repository.fullName,
                    repository.owner.login,
                    repository.owner.avatar)
        }
    }


}