package br.com.dfn.desafioandroid.features.pullrequests

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import br.com.dfn.desafioandroid.R
import br.com.dfn.desafioandroid.data.model.PullRequest
import br.com.dfn.desafioandroid.di.Injection
import br.com.dfn.desafioandroid.features.base.BaseActivity
import br.com.dfn.desafioandroid.features.base.BaseViewModel
import br.com.dfn.desafioandroid.features.pullrequests.adapters.PullRequestRecycleAdapter
import kotlinx.android.synthetic.main.activity_pull_request.*
import kotlinx.android.synthetic.main.component_loading.*


class PullRequestActivity : BaseActivity<PullRequestPresenter, PullRequestContract.PullRequestView>(),
        PullRequestContract.PullRequestView, PullRequestRecycleAdapter.OnRecycleViewListener {


    lateinit var login: String
    lateinit var rep: String
    private lateinit var mAdapter: PullRequestRecycleAdapter

    companion object {
        val loginParam = PullRequestActivity.javaClass.simpleName + ".LOGIN"
        val repParam = PullRequestActivity.javaClass.simpleName + ".REPOSITORY"
    }


    override fun onRequestLayout(): Int {
        return R.layout.activity_pull_request
    }

    override fun onCreate() {
        val mViewModel = ViewModelProviders.of(this).get(BaseViewModel::class.java)

        if (mViewModel.mPresenter == null) {
            mPresenter = PullRequestPresenter(Injection.providesAndroidSchedulers())
            mPresenter.attachLifecycle(lifecycle)
            mViewModel.mPresenter = mPresenter

        } else {
            mPresenter = mViewModel.mPresenter as PullRequestPresenter
        }

        login = intent.getStringExtra(loginParam) ?: ""
        rep = intent.getStringExtra(repParam) ?: ""

        buildToolbar()
    }

    private fun buildToolbar() {
        title = rep
    }

    override fun getView(): PullRequestContract.PullRequestView {
        return this
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        lastFirstVisiblePosition = (recyclerView.layoutManager as? LinearLayoutManager)?.findFirstCompletelyVisibleItemPosition() ?: 0
        outState?.putInt(lastPosition, lastFirstVisiblePosition)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        lastFirstVisiblePosition = savedInstanceState?.getInt(lastPosition, 0) ?: 0
    }

    override fun onResume() {
        super.onResume()
        mPresenter.doGetPullRequest(login, rep)
    }

    override fun onShowPullRequestStates(opens: Int, closes: Int) {
        val opened = getString(R.string.lbl_opened)
        val closed = getString(R.string.lbl_closed)
        txt_opened.text = "$opens $opened"
        txt_closed.text = "/  $closes $closed"
    }

    override fun onShowPullRequest(pullRequest: MutableList<PullRequest>) {
        onShowProgress(false)
        recyclerView.setHasFixedSize(false)

        val layoutManager = LinearLayoutManager(applicationContext)
        recyclerView.layoutManager = layoutManager
        mAdapter = PullRequestRecycleAdapter(pullRequest, this)
        recyclerView.adapter = mAdapter

        (recyclerView.layoutManager as LinearLayoutManager).scrollToPosition(lastFirstVisiblePosition)
    }

    override fun onClick(rep: PullRequest) {
        val uri = Uri.parse(rep.url)
        val intent = Intent(Intent.ACTION_VIEW, uri)

        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

    override fun onShowProgress(show: Boolean) {
        val visibility = if (show) View.VISIBLE else View.GONE
        loading.visibility = visibility
    }

    override fun onShowError() {
    }

    override fun onShowMessage(msg: String) {
    }


}
