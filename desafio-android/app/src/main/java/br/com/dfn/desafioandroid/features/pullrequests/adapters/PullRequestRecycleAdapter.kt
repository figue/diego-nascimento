/*
 * Copyright (C) 2018 Diego Figueredo do Nascimento.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.dfn.desafioandroid.features.pullrequests.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import br.com.dfn.desafioandroid.R
import br.com.dfn.desafioandroid.data.model.PullRequest
import br.com.dfn.desafioandroid.util.loadImage

class PullRequestRecycleAdapter(var mList: List<PullRequest>, var mListener: OnRecycleViewListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var mContext: Context

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = viewGroup!!.context
        val v1 = LayoutInflater.from(viewGroup.context).inflate(R.layout.adapter_pull_request_item, viewGroup, false)

        return CellViewHolder(v1)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val cellViewHolder = holder as CellViewHolder

        cellViewHolder.mTitleView.text = mList[position].title
        cellViewHolder.mDescriptionView.text = mList[position].body

        cellViewHolder.mLoginView.text = mList[position].user.login
        cellViewHolder.mAvatarView.loadImage(mList[position].user.avatar)

        cellViewHolder.root.setOnClickListener { mListener.onClick(mList[position]) }
    }

    private inner class CellViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val root: View = itemView.findViewById(R.id.root)
        val mTitleView: TextView = itemView.findViewById(R.id.txt_name)
        val mDescriptionView: TextView = itemView.findViewById(R.id.txt_description)
        val mLoginView: TextView = itemView.findViewById(R.id.txt_login)
        val mAvatarView: ImageView = itemView.findViewById(R.id.img_avatar)
    }

    override fun getItemCount(): Int {
        return if (mList == null) 0 else mList.size
    }

    interface OnRecycleViewListener {
        fun onClick(rep: PullRequest)
    }
}