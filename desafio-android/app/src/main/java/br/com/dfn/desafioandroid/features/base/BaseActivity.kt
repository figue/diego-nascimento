/*
 * Copyright (C) 2018 Diego Figueredo do Nascimento.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.dfn.desafioandroid.features.base

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LifecycleRegistry
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem

/**
 * Base Activity.
 *
 * This abstract class has a base implementation to Activities, all present should extend from this
 *
 * @param V the type of a interface that represent View in MVP partner.
 * @param P the type of a interface that represent Presenter in MVP partner.
 * @property mPresenter instance of Presenter attach on activity
 * @property mView instance of View attach on activity
 * @property mToolbar instance of toolbar that will be used at activity
 * @property mLifecycleRegistry instance of LifecycleRegistry, this reference will be create
 * automatically.
 */
abstract class BaseActivity<P : BasePresenter<V>, V : BaseView> : AppCompatActivity(), LifecycleOwner {

    lateinit var mPresenter: P
    private lateinit var mView: V
    private lateinit var mToolbar: Toolbar
    private lateinit var mLifecycleRegistry: LifecycleRegistry

    val lastPosition = "lastPosition"
    var lastFirstVisiblePosition: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mLifecycleRegistry = LifecycleRegistry(this)

        setContentView(onRequestLayout())
        setupToolbar()
        onCreate()
        setView(getView())
        attachPresenter(mView)
    }

    override fun onDestroy() {
        super.onDestroy()
        detachPresenter()
    }

    fun setupToolbar() {
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun setView(view: V) {
        this.mView = view
    }

    abstract fun onRequestLayout(): Int

    abstract fun onCreate()

    abstract fun getView(): V

    private fun attachPresenter(view: V) {
        mPresenter.attachView(view)
    }

    private fun detachPresenter() {
        mPresenter.detachView()
    }

    fun isConnected(): Boolean {
        val cm = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

    override fun getLifecycle(): LifecycleRegistry {
        return mLifecycleRegistry
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}