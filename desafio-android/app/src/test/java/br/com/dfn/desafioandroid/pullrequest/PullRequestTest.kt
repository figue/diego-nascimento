/*
 * Copyright (C) 2018 Diego Figueredo do Nascimento.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.dfn.desafioandroid.pullrequest

import br.com.dfn.desafioandroid.data.model.PullRequest
import br.com.dfn.desafioandroid.data.model.User
import br.com.dfn.desafioandroid.features.pullrequests.PullRequestContract
import br.com.dfn.desafioandroid.features.pullrequests.PullRequestPresenter
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


class PullRequestTest {

    private var mPresenter: PullRequestPresenter? = null
    private var mListOpens: MutableList<PullRequest>? = null
    private var mListClosed: MutableList<PullRequest>? = null
    private var mListEmpty: MutableList<PullRequest> = arrayListOf()

    @Mock
    private val mView: PullRequestContract.PullRequestView? = null

    @Before
    fun setupPullRequestPresenter() {
        MockitoAnnotations.initMocks(this)
        val testScheduler = TestScheduler()

        mPresenter = PullRequestPresenter(testScheduler)
        mPresenter!!.attachView(mView!!)

        val user = User("mock", "mock")
        val pullRequest = PullRequest(user, "mock", "mock",
                "open", "mock", "mock")
        val pullRequestClosed = PullRequest(user, "mock", "mock",
                "closed", "mock", "mock")
        mListOpens = arrayListOf(pullRequest, pullRequest, pullRequest)
        mListClosed = arrayListOf(pullRequestClosed, pullRequestClosed, pullRequestClosed)

    }

    @Test
    fun calculatePullRequestEmptyListTest() {
        mPresenter!!.calculatePullRequests(mListEmpty)

        Mockito.inOrder(mView).apply {
            verify(mView)!!.onShowPullRequestStates(0, 0)
        }
    }


    @Test
    fun calculatePullRequestWithOpensTest() {
        mPresenter!!.calculatePullRequests(mListOpens!!)

        Mockito.inOrder(mView).apply {
            verify(mView)!!.onShowPullRequestStates(3, 0)
        }
    }


    @Test
    fun calculatePullRequestWitClosesTest() {
        mPresenter!!.calculatePullRequests(mListClosed!!)

        Mockito.inOrder(mView).apply {
            verify(mView)!!.onShowPullRequestStates(0, 3)
        }
    }

    @Test
    fun loadPullRequestTest() {
        mPresenter!!.doGetPullRequest("invalid", "invalid")

        Mockito.inOrder(mView).apply {
            verify(mView)!!.onShowProgress(true)
            verify(mView)!!.onShowError()
        }
    }
}