/*
 * Copyright (C) 2018 Diego Figueredo do Nascimento.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.dfn.desafioandroid.home

import android.content.Context
import br.com.dfn.desafioandroid.data.source.local.model.RepositoryDb
import br.com.dfn.desafioandroid.features.home.HomeContract
import br.com.dfn.desafioandroid.features.home.HomePresenter
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


class HomePresenterTest {

    private var mPresenter: HomePresenter? = null
    private var mList: List<RepositoryDb>? = null
    private lateinit var rep1: RepositoryDb

    @Mock
    private val mContext: Context? = null

    @Mock
    private val mView: HomeContract.HomeView? = null

    @Before
    fun setupHomePresenter() {
        MockitoAnnotations.initMocks(this)
        val testScheduler = TestScheduler()

        mPresenter = HomePresenter(mContext!!, testScheduler)
        mPresenter!!.attachView(mView!!)

        mList = arrayListOf()

        rep1 = RepositoryDb(1, "mock", "mock", 0, 0,
                "mock", "mock", "")

    }

    @Test
    fun loadCacheRepositoryTest() {
        mPresenter!!.doRequestRepository(false)
        mPresenter?.mListRepositoryDb = arrayListOf(rep1, rep1, rep1)
        Mockito.inOrder(mView).apply {
            verify(mView)!!.onShowProgress(true)
        }
    }

    @Test
    fun loadLocalRepositoryTest() {
        mPresenter!!.doRequestRepository(false)

        Mockito.inOrder(mView).apply {
            verify(mView)!!.onShowProgress(true)
        }
    }

    @Test
    fun loadWebRepositoryTest() {
        mPresenter!!.doRequestRepository(true)

        Mockito.inOrder(mView).apply {
            verify(mView)!!.onShowProgress(true)
        }
    }
}